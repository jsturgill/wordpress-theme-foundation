<?php
namespace WTFCore\Functions;
$wtf_core_scripts = array();

// Register scripts to be included in the footer.  Every
// registered script will only be included one time
if ( ! function_exists( 'require_script' ) ) {
	function require_script( $name )
	{
		global $wtf_core_scripts;
		$url = get_bloginfo('stylesheet_directory').'/js/'.$name.'.js';
		$wtf_core_scripts[$name] = $url;
	}
}
if ( ! function_exists( 'draw_scripts' ) ) {
	function draw_scripts()
	{
		global $wtf_core_scripts;
		foreach ( $wtf_core_scripts as $name=>$url )
		{
			echo "<script type='text/javascript' src='{$url}'></script>";
		}
	}
}

// Automagically registers menus in a "theme_menus" array, if it is set.
// Generally the child theme would create the array.
if ( ! function_exists( 'register_menus' ) ) {
	function register_menus( $array )
	{
		$var = $array;
		$callback = function() use ( $var ) 
		{ 
			register_nav_menus( $var );
		};
		add_action( 'init', $callback );
	}
	if ( is_array( $theme_menus ) )
	{
		register_menus( $theme_menus );
	}
}
/* Returns the content within the specified layout.
 */
if ( ! function_exists( 'get_html' ) ) {
	function get_html($layout, $content)
	{
		// TODO: Config file/constant to set layouts directory name
		$path = get_stylesheet_directory().'/layouts/'.$layout.'.php';
	    ob_start();
	    include( $path );
	    $html = ob_get_clean();
	    return $html;
	}
}

if ( ! function_exists( 'get_layout' ) ) {
	function get_layout()
	{
		// TODO: Config file/constant to set layouts directory name
	    $root = get_stylesheet_directory().'/layouts/';
	    $paths = array();
	    if(is_tax()) {
	        $taxonomy = get_query_var('taxonomy');
	        $term = get_query_var('term');
	        $paths[] = "taxonomy/{$taxonomy}-{$term}";
	        $paths[] = "taxonomy/{$taxonomy}";
	        $paths[] = "taxonomy/taxonomy";
	    }
	    if ( is_home() )
	    {
	    	$paths[] = 'homepage';
	    }
	    
	    $post_type = get_query_var( 'post_type' );
	    if ( is_singular() )
	    {
	    	$paths[] = "$post_type-single";
	    	// TODO: add ID checks for layout of single posts
	    	$paths[] = 'default-single';
	    } else {
	    	$paths[] = "$post_type-plural";
	    	$paths[] = 'default-plural';
	    }
	    // TODO: add category check for layout
	    // if ( is_category() )
	    foreach($paths as $path)
	    {
	        if (file_exists("{$root}{$path}.php"))
	        {
	            return $path;
	        }
	    }
	    return 'default';
	}
}

if ( ! function_exists( 'generate_filters_path_array' ) )
{
	function generate_filters_path_array( $query_object )
	{
		if ( $query_object === null ) return null;
		$paths = array();
		$id = $query_object->query_vars['page_id'];
		$post_type = $query_object->query_vars['post_type'];
		$is_single = ( $query_object->is_single == FALSE ) ? FALSE : TRUE;
		// build array
		$paths[] = "{$post_type}/{$post_type}-{$id}";
		if ($is_single)
		{
			$paths[] = "{$post_type}/{$post_type}-single";
		} else {
			$paths[] = "{$post_type}/{$post_type}-plural";
		}
		$paths[] = "{$post_type}/{$post_type}-default";
		if ($is_single)
		{
			$paths[] = "default-single";
		} else {
			$paths[] = "default-plural";
		}
		$paths[] = "default";
		return $paths;
	}	
}

if ( ! function_exists( 'generate_view_path_array' ) ) 
{
	function generate_view_path_array( $post_object, $is_single = true, $is_tax = false )
	{
		$paths = array();
		$id = $post_object->ID;
		$post_type = get_post_type( $post_object );
		$post_custom_values = get_post_custom( $id );
		// TODO: Config file/constant to set view variable for WPF selector integration
		$view = $post_custom_values['select_view'];
		$view = ( is_array($view) && array_key_exists( 0, $view ) ) ? $view[0] : false;
		if($is_tax) {
			$taxonomy = get_query_var('taxonomy');
			$term = get_query_var('term');
			if ($template) $paths[] = $template;
			$paths[] = "{$post_type}/{$taxonomy}-{$term}-{$id}";
			$paths[] = "{$post_type}/{$taxonomy}-{$term}";
			$paths[] = "{$post_type}/{$taxonomy}-{$id}";
			$paths[] = "{$post_type}/{$taxonomy}";
			$paths[] = "{$post_type}/taxonomy-{$id}";
			$paths[] = "{$post_type}/taxonomy";
			if ($view) $paths[] = "{$post_type}/$view";
			$paths[] = "{$post_type}/{$post_type}-{$id}";
			$paths[] = "{$post_type}/{$post_type}-default";
			$paths[] = "{$taxonomy}-{$term}-{$id}";
			$paths[] = "{$taxonomy}-{$term}";
			$paths[] = "{$taxonomy}-{$id}";
			$paths[] = "{$taxonomy}";
			$paths[] = "taxonomy-{$id}";
			$paths[] = "taxonomy";
			$paths[] = "default";
		} else {
			if ($template) $paths[] = $template; // passed-in view overrides everything
			$paths[] = "{$post_type}/{$post_type}-{$id}"; // ID-specified template overried view selection
			if ($view) $paths[] = "{$post_type}/$view"; // specified view shortcuts typical delegation
			if ($is_single)
			{
				$paths[] = "{$post_type}/{$post_type}-single";
			} else {
				$paths[] = "{$post_type}/{$post_type}-plural";
			}
			$paths[] = "{$post_type}/{$post_type}-default";
			if ($is_single)
			{
				$paths[] = "default-single";
			} else {
				$paths[] = "default-plural";
			}
			$paths[] = "default";
		}
		return $paths;
	}
}

if ( ! function_exists( 'process_view_path_array' ) ) {
	function process_view_path_array( $paths )
	{
		// TODO: Config file/constant to set views directory name
		$root = get_stylesheet_directory().'/views/';
		if ( ! is_array( $paths ) )
		{
			// TODO: add a configuration flag for dev vs. production.
			// The paths error below should never be displayed in production
			echo '<p>Malformed paths array.  Unable to display the post.';
		}
		foreach($paths as $path)
		{
			if (file_exists("{$root}{$path}.php"))
			{
				// echo "$root$path found.<br />"; // uncomment to troubleshoot
				include("{$root}{$path}.php");
				return;
			} else {
				// echo "$root$path not found.<br />"; // uncomment to troubleshoot
			}
		}
		// TODO: add a configuration flag for dev vs. production.
		// The paths array should never be displayed in production
		echo '<p>No template found.  A template can be placed in any of the following locations:<br />
				(Root path: '.$root.' )</p>';
		pre($paths);
		return;
	}
}

/* Runs the WordPress loop and returns page content based on post type.
 */
if ( ! function_exists( 'get_content' ) ) {
	function get_content( $template = null, $post_object = null )
	{ 
		global $post;
	    ob_start();
	    if ( $post_object === null && have_posts() === true ) {
	        while ( have_posts()) {
	    		the_post();
	    		$paths = generate_view_path_array( $post, is_single(), is_tax() );
	    		if ( $template ) array_unshift( $paths, get_post_type() . '/' . $template, $template );
	    		process_view_path_array( $paths );
			} 
	    } elseif ( is_object( $post_object ) ) { 
	    	$holder = $post;
	    	$post = $post_object;
	    	setup_postdata( $post );
	    	$paths = generate_view_path_array( $post ); // is_single = true, is_tax = false
	    	if ( $template ) array_unshift( $paths, get_post_type( $post_object ) . '/' . $template, $template );
	    	process_view_path_array( $paths );
	    	$post = $holder;
	    	wp_reset_query();
		} else {
			// TODO: Config file or constant to set no results view
			$root = get_stylesheet_directory().'/views/';
	        include("{$root}no_results.php");
		}
	    return $content = ob_get_clean();
	}
}

/* Prints the specified HTML chunk
 */
if ( ! function_exists( 'render_chunk' ) ) {
	function render_chunk( $name )
	{
		// TODO: Config file/constant to set chunks directory name
	    $path = get_stylesheet_directory().'/views/chunks/'.$name.'.php';
	    if (file_exists($path)) { include($path); }
	}
}

function pre($variable)
{
    echo "<pre>";
    print_r($variable);
    echo "</pre>";
}

if ( ! function_exists( 'render_view' ) ) {
	function render_view($view, $variables)
	{
		// TODO: Config file/constant to set views directory name
	    extract($variables);
	    include(get_stylesheet_directory().'/views/'.$view.'.php');
	}
}

// Use this function as the source of a pulldown field
// in a metabox to allow views to be selected for a post type
// if using WordPress Plugin Foundation code
// See http://jeremiahsturgill.com for more information about WPF
// Example manifest:
/* - ID: view_selector
     Name: select_view_meta_box
     Title: Select View
     Post Type: page
     Context: side
     Fields:
     - Name: select_view
       Title: Select View
       Type: pulldown
       Source: \WTFCore\Functions\view_selector_options
/**/
if ( ! function_exists( 'view_selector_options' ) ) {
	function view_selector_options($field)
	{
		$post_type = get_post_type();
		$path = get_stylesheet_directory().'/views/'.$post_type.'/';
		$failure = array(array(
				'value' =>'',
				'text' =>'No views to choose from',)); // default value for failure
		$handle = opendir($path);
		$views = array();
		if ($handle !== false)
		{
			while ( false !== ($entry = readdir( $handle ) ) ) {
				if( $entry !=='.' && $entry !== '..' )
				{
					// Simple cleanup of filename to limit shenanigans possible
					// from a malitious filename.
					$option_value = str_replace(array('/', '\\', '.php', '.'), '', $entry);
					$option_display = esc_attr(str_replace('_', ' ', $option_value));
					$views[] = array('value' => $option_value,
									 'text' => $option_display);
				}
			}
		}
		return ( empty($views) ) ? $failure : $views; /**/
	}
}

// WPF integration helper.
// Takes an array, a key, and an index.
// Returns either false or a value
if ( ! function_exists( 'get_array_entry' ) ) {
	function get_array_entry($custom, $key, $index = 0)
	{
		$default = array( $key => array( $index => false ) );
		$custom = array_merge( $default, (array)$custom );
		if ($index >= 0 )
		{
			return maybe_unserialize( $custom[$key][$index] );
		} else {
			foreach( $custom[$key] as $the_value)
			{
				$result[] = maybe_unserialize( $the_value );
			}
			return $result;
		}
	}
}

// Repurpose the view array to check for filters
if ( ! function_exists( 'process_filters' ) )
{
	function process_filters( $query_object = null )
	{
		$paths = \WTFCore\Functions\generate_filters_path_array( $query_object );
		if ( is_array( $paths ) )
		{
			$root = get_stylesheet_directory().'/filters/';
			// all filters will be included, not just the first found
			foreach ( $paths as $path )
			{
				if (file_exists( "{$root}{$path}.php" ))
				{
					include_once( "{$root}{$path}.php"	 );
				}
			}
		}
	}
}

// before the query is run
add_action( 'pre_get_posts', '\WTFCore\Functions\process_filters' );
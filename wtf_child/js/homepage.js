(function(){
	jQuery(document).ready(function(){
		var initial_block_width_css = '30%';
		var smallest_block_size = Math.ceil( jQuery( '.content_block' ).first().outerWidth() );
		var row_limit = jQuery( '.homepage' ).children('.content_block').length;
		var block_size, container_width, possible_rows, content_width, new_width;
		function resize()
		{
			block_size = Math.ceil( jQuery( '.content_block' ).first().outerWidth() );
			if ( block_size < smallest_block_size )
			{
				smallest_block_size = block_size;
			}
			container_width = Math.floor( jQuery( '.content' ).innerWidth() );
			possible_rows = Math.floor( container_width / smallest_block_size );
			if ( possible_rows > row_limit )
			{
				possible_rows = row_limit;
			}
			if ( possible_rows > 1 )
			{
				new_width = Math.floor( 100 / possible_rows ) + '%';
				if ( jQuery( '.content_block' ).first().css( 'width' ) != new_width )
				{
					jQuery( '.content_block' ).css( 'width', new_width);
				}
				content_width = possible_rows * Math.ceil( jQuery( '.content_block' ).first().outerWidth() );
				jQuery( '.content' ).css('padding-left', Math.floor( ( container_width - content_width ) / 2 ) + 'px');
			} else {
				jQuery( '.content' ).css( 'padding-left', '0' );
				jQuery( '.content_block' ).css( 'width', 'auto' );
			}
		}
		resize();
		jQuery( window ).resize(function(){
			resize();
		});
	});
})();
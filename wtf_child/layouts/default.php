<?php 
use WTFCore\Functions;
get_header();?>
<body <?php 
  body_class(); 
?>><div class="header"><?php 
  Functions\render_chunk('header');
?></div><div id="content_wrap">
    <div class="content">
        <?=$content;?>
    </div>
</div><div class="footer"><?php 
  Functions\render_chunk('footer');
?></div><script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script><?php
  get_footer();
?></body>
</html>
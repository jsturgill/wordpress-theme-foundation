<div id="page-<?php the_ID(); ?>" <?php post_class( 'textfield' ); ?>>
    <div class="title"><a title="permalink" href="<?=the_permalink();?>"><?=the_title();?></a></div>
    <div class="body"><?=the_content();?></div>
</div>
<?php
	$project_args = array(
			'posts_per_page' => -1,
			'post_type' => 'project',
	);
	$post_args = array(
			'post_type' => 'post',
	);
	query_posts( $project_args );
	$projects = \WTFCore\Functions\get_content();
	query_posts( $post_args );
	$posts = \WTFCore\Functions\get_content();
	wp_reset_query();
?><div class="homepage"><div class="project_list content_block">
	<h3>Projects</h3>
	<?=$projects;?>
</div><div class="posts content_block">
	<h3>Thoughts</h3>
	<?=$posts?>
</div><?php 
	$pages = get_option('js_site_frontpage_content_blocks', false);
	$page_array = maybe_unserialize( $pages );
	if ( $pages != false && is_array( $page_array ) ) :
		foreach ( $page_array as $data ) :
			$args = array( 'page_id' => \WTFCore\Functions\get_custom_entry( $data, 'page' ) );
			query_posts( $args );
			$template = \WTFCore\Functions\get_custom_entry( $data, 'content_template' );
			$template = ( $template ) ? $template : 'embed';
			$title = \WTFCore\Functions\get_custom_entry( $data, 'title' );
			$content = \WTFCore\Functions\get_content( $template = $template );
			?><div class="embedded_page content_block"><h3><?=esc_html( $title );?></h3><?=$content;?></div><?php 
		endforeach;
		wp_reset_query();
	endif;
?></div><?php WTFCore\Functions\require_script( 'homepage' );?>
<div id="post-<?php the_ID(); ?>" <?php post_class( 'plural textfield' ); ?>>
    <div class="title"><a title="permalink" href="<?=the_permalink();?>"><?=the_title();?></a></div>
    <div class="excerpt"><?=the_excerpt();?></div>
    <div class="meta"><a href="<?=the_permalink();?>">more &raquo;</a></div>
</div>
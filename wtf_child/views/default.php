<?php 
if ( is_single() ) 
{
	$class = 'single textfield';
} else {
	$class = 'plural textfield';
}
?><div id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
    <div class="title"><a title="permalink" href="<?=the_permalink();?>"><?=the_title();?></a></div>
    <div class="body"><?=the_content();?></div>
</div>
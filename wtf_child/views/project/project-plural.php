<?php 
$custom = get_post_custom();
$languages = $custom['languages'];
$repository = \WTFCore\Functions\get_custom_entry( $custom, 'repository_url' );
$live = \WTFCore\Functions\get_custom_entry( $custom, 'live_url' );
$documentation = \WTFCore\Functions\get_custom_entry( $custom, 'documentation_url' );
$screenshots = \WTFCore\Functions\get_custom_entry( $custom, 'screenshots', $index = -1 );
$screencasts = \WTFCore\Functions\get_custom_entry( $custom, 'screencasts', $index = -1 );
$infrastructure = \WTFCore\Functions\get_custom_entry( $custom, 'infrastructure', $index = -1 );
// \WTFCore\Functions\pre( $custom );
?><div id="project-<?php the_ID(); ?>" <?php post_class( 'plural textfield' ); ?>>
    <div class="title"><a title="permalink" href="<?=the_permalink();?>"><?=the_title();?></a></div>
    <div class="excerpt lowlight"><?=the_excerpt();?></div>
    <div class="meta lowlight offset_block">
    	<ul class="main_list">
    		
    		<?php if ( $repository ) : ?><li class="repository_url"><a href="<?=$repository;?>">repository</a></li><?php endif;?>
    		<?php if ( $live ) : ?><li class="live_preview"><a href="<?=$live;?>">see it live</a></li><?php endif;?>
    		<?php if ( $documentation ) : ?><li class="documentation"><a href="<?=$documentation;?>">documentation</a></li><?php endif;?>
    	</ul>
    	<ul class="language_list">
    		
    		<?php 
    		foreach( $languages as $language )
    		{
    			echo "<li>$language</li>";
    		}
    		foreach ($infrastructure as $structure )
    		{
    			echo "<li>$structure</li>";
    		}
    		?>
    	</ul>
    </div>
</div><?php 
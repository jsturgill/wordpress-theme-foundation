<?php 
$custom = get_post_custom();
$languages = $custom['languages'];
$repository = \WTFCore\Functions\get_custom_entry( $custom, 'repository_url' );
$live = \WTFCore\Functions\get_custom_entry( $custom, 'live_url' );
$download = \WTFCore\Functions\get_custom_entry( $custom, 'download_url' );
$documentation = \WTFCore\Functions\get_custom_entry( $custom, 'documentation_url' );
$screenshots = \WTFCore\Functions\get_custom_entry( $custom, 'screenshots', $index = -1 );
$screencasts = \WTFCore\Functions\get_custom_entry( $custom, 'screencasts', $index = -1 );
$infrastructure = \WTFCore\Functions\get_custom_entry( $custom, 'infrastructure', $index = -1 );
if ( is_single() )
{
	$class = 'single textfield';
} else {
	$class = 'plural textfield';
}
// \WTFCore\Functions\pre( $custom );
?><div id="project-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
    <div class="title"><a title="permalink" href="<?=the_permalink();?>"><?=the_title();?></a></div>
    <div class="body"><?=the_content();?></div>
    <div class="meta lowlight offset_block">
    	<ul class="main_list"><?php
    		if ( $repository ) : ?><li class="repository_url"><a href="<?=$repository;?>">repository</a></li><?php endif;
    		if ( $live ) : ?><li class="live_preview"><a href="<?=$live;?>">see it live</a></li><?php endif;
    		if ( $documentation ) : ?><li class="documentation"><a href="<?=$documentation;?>">documentation</a></li><?php endif;
    		if ( $download ) : ?><li class="download"><a href="<?=$download;?>">download</a></li><?php endif;
    	?></ul>
    	<ul class="language_list">
    		<?php 
    		foreach( $languages as $language )
    		{
    			echo "<li>$language</li>";
    		}
    		foreach ($infrastructure as $structure )
    		{
    			echo "<li>$structure</li>";
    		}
    		?>
    	</ul>
    </div>
</div><?php 